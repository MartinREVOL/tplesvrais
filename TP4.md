# **Rocky Linux TP4**

➜ **Configuration IP statique (fait en cours)** : 
- sudo nmcli con reload
- sudo nmcli con up <INTERFACE>
- sudo nmcli con up enp0s8
    
➜ **Verifier si la configuration est bonne** : 
- **cat /etc/sysconfig/network-scripts/ifcfg-enp0s8** : 
    TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
IPADDR=10.250.1.53
ONBOOT=yes
NETMASK=255.255.255.0
- **ip a** : 
    inet 10.250.1.53/24
    
➜ **Connexion SSH fonctionnelle** : 
- le service ssh est actif sur la VM : 
    - **systemctl status sshd** : 
       Active: active (running) since Tue 2021-11-23 15:57:45 CET; 10h ago
- échange de clés : 
    - on génère une clef avec --> **ssh-keygen -t rsa -b 4096**
    - on dépose la clef sur le serveur avec --> **ssh-copy-id <USER>@<IP>**
    - et enfin, on se connecte sur le serveur avec --> **ssh <USER>@<IP>**
    
   Et PAF ! On a pas besoin de mettre de mot de passe car on a la clef :))))
    
Et donc voilà ce que ça m'afficher lorsque les manipulations on été faites et que je me connecte à mon serveur : 
    
    Last login: Tue Nov 23 17:10:28 2021 from 10.250.1.1
    
➜ **Accès internet** : 
- avoir de la connection internet : 
    on fait un "**ping 1.1.1.1**" (le 1.1.1.1 est préférable au 8.8.8.8 qu'on peux voir souvent ^^^,
    et ça nous donne une succession de lignes comme celle-ci --> 64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=34.1 ms
- avoir de la résolution de nom : 
    Bah pareil du coup un petit **ping** +, par exemple, **.fr** ça passe : 
    26 packets transmitted, 0 received, 100% packet loss, time 25703ms
    
➜ **Nommage de la machine** : 
- changer son nom : 
    Avec **sudo hostname <NOM>**
- écrire son nom dans un fichier : 
    Là on va mettre son nom dans **/etc/hostname**, donc la commande va être : **sudo nano /etc/hostname**

 Et donc, pour vérifier on fait un **cat**, et ça nous donne : Rocky.Linux.TP4 (c'est le nom que j'ai mis pour la machine)

 D'ailleur, on peux aussi verifier avec **"hostname"**, et ça devrait nous donner la même chose qu'avec un **cat** (Rocky.Linux.TP4 pour moi).
    
    
# **Mise en place d'un service (yeaaar)**
    
**1. On va installer NGINX** :+1: 
