# **TP5 : P'tit cloud perso**

## 0. Prérequis

### Checklist :
- la VM a un accès internet : 
    - **ping 1.1.1.1** fonctionnel : 
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 30.089/31.284/33.195/1.372 ms
    - **ping ynov.com** fonctionnel : 
--- ynov.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
rtt min/avg/max/mdev = 25.062/33.356/43.529/7.655 ms
- SELinux est désactivé : 
    - la commande **sestatus** doit retourner : Current Mode: permissive : 
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
**Current mode:                   permissive**
Mode from config file:          permissive
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
- la machine est nommée (avec **hostname** en commande): 
web.tp5.linux ou db.tp5.linux en fonction de la machine...

## I. Setup DB

### 1. Install MariaDB

#### 🌞 Installer MariaDB sur la machine db.tp5.linux
- **sudo dnf install mariadb-server**
- **sudo dnf install mariadb**

#### 🌞 Le service MariaDB
- **sudo systemctl start mariadb**
- **sudo systemctl enable mariadb**
- **systemctl status mariadb**
Active: active (running) since Tue 2021-11-23 17:12:22 CET; 5s ago
- **sudo ss -tulnp | grep mysqld | awk '{print $5}'** == numéro du port : 
*:3306
- **sudo ss -tulnp | grep sqld | cut -d'"' -f2** == le processus : 
mysql
- **ps -ef | grep mysqld | awk '{print $1}'** == les processus liés au service MariaDB : 
mysql
toto
- l'utilisateur où est lancé le process MariaDB : 
mysql

#### 🌞 Firewall
- Lancer Firewall ----> **sudo systemctl start firewalld**
- ouverture du port utilisé par MySQL ----> **sudo firewall-cmd --zone=public --permanent --add-port=3306/tcp** : 
success
- Pi il faut le reload : **sudo firewall-cmd --reload**

### 2. Conf MariaDB

#### 🌞 Configuration élémentaire de la base
-----> **mysql_secure_installation**
- **Question 1** --> **Set root password?**
Alors... OUI ! Mettre un mot de pass au root est une plutôt bonne idée si on veut éviter que tout le monde se mette à taper des commandes à droite et à gauche.
- **Question 2** --> **Remove anonymous users?**
OUI ! Je préfère qu'il n'y ai pas d'anonymes... Étant donné qu'en plus je suis censé être le seul dans mon "cloud".
- **Question 3** --> **Disallow root login remotely?**
OUI ! Question sécurité, c'est une bonne chose d'interdire la connexion de root à distance.
- **Question 4** --> **Remove test database and access to it?**
OUI ! La base de données de test ne me servira à rien... Elle sera donc un potentiel point faible à exploiter pour les "Hackers en chemises" !
- **Question 5** --> **Reload privilege tables now?**
Alors ça en gros ça veux dire : Voulez-vous que toutes les modifications apportées jusqu'à présent
prennent effet immédiatement ? Et du coup, bah, OUI !

#### 🌞 Préparation de la base en vue de l'utilisation par NextCloud
Bon... Pour faire ça tout a été clairement expliqué dans le TP de Léo(toi mon gars), donc bah voici un extrait de ce que tu as écrit :))) 
- Connexion à la base de données
- L'option -p indique que vous allez saisir un mot de passe
- Vous l'avez défini dans le mysql_secure_installation
**sudo mysql -u root -p**

Bah du coup pour la suite c'est pareil, tu l'as déjà fait dans le TP : 
- **CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';**
- **CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;**
- **GRANT ALL PRIVILEGES ON nextcloud.*** **TO 'nextcloud'@'10.5.1.11';**
- **FLUSH PRIVILEGES;**

### 3. Test

#### 🌞 Installez sur la machine web.tp5.linux la commande mysql
Pour ça on va taper la commande : **dnf provides mysql**
ça nous donne : 
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Et pour finir on l'installe : **dnf install mysql** : 
Terminé !

#### 🌞 Tester la connexion

**mysql --host=10.5.1.12 --port=3306 -p --user=nextcloud nextcloud**

Et là PAM ! Tu fais un "bête" **SHOW TABLES;**
Et du coup c'est vide... Normal quoi :
Empty set (0,01 sec)

## II. Setup Web

### 1. Install Apache

#### A. Apache

##### 🌞 Installer Apache sur la machine web.tp5.linux
Maintenant on sait faire ça, en plus, vu que le paquet et le service s'appellent tout les deux "httpd", alors on a juste à faire : 
**sudo dnf install httpd**

##### 🌞 Analyse du service Apache
- lancez le service httpd --> **sudo systemctl start httpd**
- Activez le au démarrage --> **sudo systemctl enable httpd** : 
On peux verifier ça avec **systemctl status httpd** qui retourne : 
Active: active (running) since Tue 2021-11-23 21:15:32 CET; 4min 3s ago
- Isolez les processus liés au service httpd --> **ps -ef | grep httpd | cut -d" " -f1** : 
root
apache
apache
apache
apache
toto
- Déterminez sur quel port écoute Apache par défaut --> **sudo ss -tulnp | grep httpd | awk '{print $5}'** : 
80
- Déterminez sous quel utilisateur sont lancés les processus Apache -->  Bah du coup c'est "apache" l'utilisateur :)

##### 🌞 Un premier test
- Ouvrez le port d'Apache dans le firewall --> **sudo firewall-cmd --zone=public --permanent --add-port=80/tcp**
- Reload firewall --> **sudo firewall-cmd --reload**
- On va accéder à la page d'accueil par défaut d'Apache  : 
    - **curl 10.5.1.11** : 
Last login: Thu Nov 25 17:00:24 on ttys005
    - On tape l'IP dans le navigateur et ça nous ouvre une page, que j'ai pas envie de décrire ptdr... Bon ok je vais vite fait le faire : 
        - C'est tout vert !!!
        - Et y'a écrit en GROS "HTTP Server Test Page"
    Voala...
    
#### B. PHP

##### 🌞 Installer PHP
Faut faire les commandes du TP pour installer PHP.

### 2. Conf Apache

#### 🌞 Analyser la conf Apache

