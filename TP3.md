# I. Carte d'identité (en entrée)

Pour lancer le script, il faut taper la commande : **bash /srv/idcard/idcard.sh**

**echo "Machine name : $(hostname)"**

**echo** == retour à la ligne

**echo "OS $(cat /etc/os-release | grep PRETTY | cut -d"=" -f2) and kernel version is $(uname -v)"

echo

echo "IP : $(ip a | grep 192 | awk '{print$2}')"

echo

echo "RAM : $(free -mh | grep Mem | awk '{print $7}')/$(free -mh | grep Mem | awk '{print $2}')"

echo

echo "Disque : $(df -H | grep /dev/sda5 | awk '{print $4}')o space left"

echo

echo "Top 5 processes by RAM usage :"

echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 5 | head -n 1)"

echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 2 | head -n 1)"

echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 3 | head -n 1)"

echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 4 | head -n 1)"

echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 6 | head -n 1)"

echo

echo "Listening ports :"

ya=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 1) : $ya"

sa=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 2 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 2 | tail -n 1) : $sa"

da=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 3 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 3 | tail -n 1) : $da"

ra=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 4 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 4 | tail -n 1) : $ra"

ua=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 5 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 5 | tail -n 1) : $ua"

ma=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 6 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 6 | tail -n 1) : $ma"

echo

echo "Pour utiliser curl veuillez l'installer avec la commande : sudo apt install curl"

echo

ta=$(curl https://api.thecatapi.com/v1/images/search 2> /dev/null | cut -d'"' -f10)

echo "Here's your random cat : $ta"**





I. Carte d'identité (en sortie)


Machine name : toto-VirtualBox

OS "Ubuntu 20.04.3 LTS" and kernel version is #44~20.04.2-Ubuntu SMP Tue Oct 26 18:07:44 UTC 2021

IP : 192.168.56.115/24

RAM : 249Mi/974Mi

Disque : 1,4Go space left

Top 5 processes by RAM usage :
- 35.1    1047 /usr/bin/gnome-shell
-  5.4     781 /usr/lib/xorg/Xorg vt2 -displayfd 3 -auth /run/user/1000/gdm/Xauthority -background none -noreset -keeptty -verbose 3
-  4.5    1702 update-notifier
-  4.5    1503 /usr/libexec/gnome-terminal-server
-  1.8    1104 /usr/libexec/ibus-extension-gtk3

Listening ports :
[sudo] Mot de passe de toto : 
- 4096 : systemd-resolve
- 128 : sshd
- 5 : cupsd
- 32 : vsftpd
- 128 : sshd
- 5 : cupsd

Pour utiliser curl veuillez l'installer avec la commande : sudo apt install curl

Here's your random cat : https://cdn2.thecatapi.com/images/fEXwKXJ_n.png







II. youtube.dl (en entrée)


video_url="$1"
video_name=$(youtube-dl --get-title $video_url)
youtube-dl -o "/srv/yt/downloads/$video_name/%(title)s.%(ext)s" $video_url > /dev/null

Pour lancer le script, il faut taper la commande : bash -x /srv/yt/yt.sh <URL_VIDEO>




II. youtube.dl (en sortie)


+ video_url='https://www.youtube.com/watch?v=SA1hipDADPQ'
++ youtube-dl --get-title 'https://www.youtube.com/watch?v=SA1hipDADPQ'
+ video_name='OWL BIRDS🦉- A Funny Owls And Cute Owls Videos Compilation (2021) #003 || Funny Pets Life'
+ youtube-dl -o '/srv/yt/downloads/OWL BIRDS🦉- A Funny Owls And Cute Owls Videos Compilation (2021) #003 || Funny Pets Life/%(title)s.%(ext)s' 'https://www.youtube.com/watch?v=SA1hipDADPQ'
